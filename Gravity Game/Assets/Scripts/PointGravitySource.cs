﻿using UnityEngine;
using System.Collections;

public class PointGravitySource : MonoBehaviour {

	public float magnitude = 9.8f;
	public float radius = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate (){
		PointAttraction ();
	}

	void PointAttraction(){
		Vector4 positionAndMagnitude = new Vector4 (transform.position.x,
		                                           transform.position.y,
		                                           transform.position.z,
		                                           magnitude);
		Object[] objects = FindObjectsOfType (typeof(GameObject));
		foreach (GameObject block in objects) {
			block.SendMessage ("AttractToPoint", positionAndMagnitude, SendMessageOptions.DontRequireReceiver);
		}
	}
}
