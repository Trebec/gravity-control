﻿using UnityEngine;
using System.Collections;

public class ObjectGravity : MonoBehaviour {

	public Vector3 localGravity;

	// Use this for initialization
	void Start () {
		if (localGravity == Vector3.zero) {
			localGravity = Vector3.down * 9.8f;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		GetComponent<Rigidbody>().velocity += localGravity * Time.fixedDeltaTime;
	}
}
