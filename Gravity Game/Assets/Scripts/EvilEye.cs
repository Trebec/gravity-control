﻿using UnityEngine;
using System.Collections;

public class EvilEye : MonoBehaviour {

	public ParticleEmitter effect;
	public int maxRange = 40;
	public int arcWidth = 60;
	public int arcHeight = 60;
	public float resolution = 0.1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Quaternion downDegree = Quaternion.AngleAxis (1, transform.right);
		Quaternion rightDegree = Quaternion.AngleAxis (1, transform.up);
		Vector3 currentDirection = transform.forward;
		for (float i = -arcHeight; i < arcHeight; i+=(1/resolution)) {
			downDegree = Quaternion.AngleAxis (i, transform.right);
			for(float j = -arcWidth; j < arcWidth; j+=(1/resolution)){
				rightDegree = Quaternion.AngleAxis (j, transform.up);
				currentDirection = downDegree * rightDegree * transform.forward;
				RaycastHit hit;
				if(Physics.Raycast(transform.position, currentDirection, out hit, maxRange)){
					Debug.DrawLine (transform.position, hit.point, Color.red, 0.1f);
					//then fill the line with particles
				} else {
					Vector3 endPoint = transform.position + (currentDirection.normalized * maxRange);
					Debug.DrawLine (transform.position, endPoint, Color.red, 0.1f);
					//fill space from origin to maxRange
				}
			}
		}
	}
}
