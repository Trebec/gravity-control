﻿using UnityEngine;
using System.Collections;

public class CharacterGravity : MonoBehaviour {

	public float damping;
	public Vector3 localGravity;

	// Use this for initialization
	void Start () {
		if (damping == 0){damping = 5;}
		if (localGravity == Vector3.zero){localGravity = Vector3.down * 9.8f;}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("Fire1")) {
			RaycastHit hit;
			if(Physics.Raycast(transform.position, transform.forward, out hit)){
				localGravity = -hit.normal;
			}
		}
		transform.up = Vector3.Slerp(transform.up, -localGravity, Time.deltaTime * damping);
	}

	void FixedUpdate(){
		GetComponent<Rigidbody>().velocity += localGravity * Time.fixedDeltaTime;
	}
}
