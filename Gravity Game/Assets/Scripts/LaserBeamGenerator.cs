﻿using UnityEngine;
using System.Collections;

public class LaserBeamGenerator : MonoBehaviour {

	float maxRange = 40;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		if(Physics.Raycast(transform.position, transform.forward, out hit, maxRange)){
			Debug.DrawLine (transform.position, hit.point, Color.red, 0.1f);
			//then fill the line with particles
		} else {
			Vector3 endPoint = transform.position + (transform.forward.normalized * maxRange);
			Debug.DrawLine (transform.position, endPoint, Color.red, 0.1f);
			//fill space from origin to maxRange
		}
	}
}
