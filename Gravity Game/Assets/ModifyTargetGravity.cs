﻿using UnityEngine;
using System.Collections;

public class ModifyTargetGravity : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("Fire2")) {
			ObjectGravity targetGravity = null;
			RaycastHit hit;
			if(Physics.Raycast(transform.position, transform.forward, out hit)){
				targetGravity = hit.rigidbody.gameObject.GetComponent<ObjectGravity>();
			}
			if(targetGravity != null){
				Vector3 nextGravity;
				CharacterGravity thisGravity = transform.gameObject.GetComponent<CharacterGravity>();
				ObjectGravity alternateGravity = transform.gameObject.GetComponent<ObjectGravity>();
				if(thisGravity != null){
					nextGravity = thisGravity.localGravity;
				} else if(alternateGravity != null){
					nextGravity = alternateGravity.localGravity;
				} else nextGravity = Vector3.down * 9.8f;
				targetGravity.localGravity = nextGravity;
			}
		}
	}
}
