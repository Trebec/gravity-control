﻿using UnityEngine;
using System.Collections;

public class RespawnPointScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag("Player")) {
			Object[] objects = FindObjectsOfType (typeof(GameObject));
			foreach (GameObject block in objects) {
				block.SendMessage ("CheckpointSaveData", SendMessageOptions.DontRequireReceiver);
			}
			Destroy(this);
		}
	}
}
