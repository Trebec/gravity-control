﻿using UnityEngine;
using System.Collections;

public class TCharacterGravity : MonoBehaviour {

	public bool canAttractObjects = true;
	public bool canClickGravity = true;
	public Camera playerCam;
	public float cameraSpeed = 100;
	public int maxPitch = 85;
	public int minPitch = -85;
	public float damping;
	public Vector3 localGravity = Vector3.zero;
	public float moveSpeed = 8.0F;
	public float jumpSpeed = 6.0F;
	private float pitch = 0.0F;
	private float yaw = 0.0F;
	private bool isGrounded = true;
	//Variables to load on checkpoint
	private Vector3 checkpointGravity = Vector3.zero;
	private float checkpointPitch = 0.0F;
	private float checkpointYaw = 0.0F;
	private Vector3 checkpointPosition;
	private Quaternion checkpointRotation;

	void Start () {
		if (damping == 0){damping = 5;}
		if (localGravity == Vector3.zero){localGravity = new Vector3(0, -9.8f, 0);}
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		Cursor.visible = false;
		checkpointPosition = transform.position;
		checkpointRotation = transform.rotation;
	}

	void Update () {
		GravityFunction ();
		AlignToGravity ();
		CameraControls ();
		MovementControls ();
		AttractObject ();
	}

	void FixedUpdate(){
		GetComponent<Rigidbody>().velocity += localGravity * Time.fixedDeltaTime;
	}

	void GravityFunction(){
		if (Input.GetKeyDown(KeyCode.Mouse0) && canClickGravity == true) {
			RaycastHit hit;
			if(Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit)){
				localGravity = -hit.normal * 9.8f;
				Debug.DrawLine (transform.position, hit.point, Color.cyan, 2);

				Object[] objects = FindObjectsOfType (typeof(GameObject));
				foreach (GameObject block in objects) {
					block.SendMessage ("GravityChanged", localGravity, SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}

	void AlignToGravity(){
		Transform oldTransform = playerCam.transform;
		Vector3 oldForward = oldTransform.forward;
		float oldAngle = Vector3.Angle (transform.up, oldForward);


		Vector3 currentUp = Vector3.Slerp (transform.up, -localGravity, Time.deltaTime * damping);
		Vector3 facing = Vector3.ProjectOnPlane (oldTransform.forward, currentUp);
		transform.rotation = Quaternion.LookRotation (facing, currentUp);

		//print (oldForward - playerCam.transform.forward);

		//Adjust camera angle
		Vector3 cameraUp = Vector3.Cross (oldForward, playerCam.transform.right);
		playerCam.transform.rotation = Quaternion.LookRotation (oldForward, cameraUp);
		//Good!

		/*
		//check change in rotation. if it's near 180, manually reset.
		float difference = Quaternion.Angle (oldTransform.rotation, transform.rotation);
		if(difference > 160){
			print(difference);
			Quaternion bodyRotation = Quaternion.Euler (0, 180, 0);
			transform.rotation *= bodyRotation;
		}*/


		float newAngle = Vector3.Angle (transform.up, playerCam.transform.forward);
		//Need to figure out the forced rotation and add it to the the pitch
		//Problem: Always gives positive angle
		pitch += newAngle - oldAngle; //Test
		print (newAngle - oldAngle);
	}

	void MovementControls(){
		Vector3 moveDirection = transform.forward * Input.GetAxis("Vertical");
		moveDirection += transform.right * Input.GetAxis("Horizontal");
		moveDirection = moveDirection.normalized * moveSpeed;
		//if (Input.GetButton ("Jump")) {moveDirection.y = jumpSpeed;}
		GetComponent<Rigidbody>().AddForce (moveDirection);
		//rigidbody.velocity = moveDirection * Time.deltaTime;
	}

	void CameraControls(){
		pitch += Input.GetAxis("Mouse Y") * -cameraSpeed * Time.deltaTime;
		float localYaw = Input.GetAxis("Mouse X") * cameraSpeed * Time.deltaTime;
		yaw += localYaw;
		pitch = Mathf.Clamp (pitch, minPitch, maxPitch);
		Quaternion camRotation = Quaternion.Euler (pitch, 0, 0);
		playerCam.transform.localRotation = camRotation;
		Quaternion bodyRotation = Quaternion.Euler (0, localYaw, 0);
		transform.rotation *= bodyRotation;
	}

	void AttractObject(){
		if (Input.GetKeyDown(KeyCode.Mouse1) && canAttractObjects == true) {
			RaycastHit hit;
			if(Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit)){
				Debug.DrawLine (transform.position, hit.point, Color.yellow, 2);
				hit.rigidbody.gameObject.SendMessage ("Attract", localGravity, SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	void AttractToPoint(Vector4 targetAndMagnitude){
		Vector3 target = new Vector3 (targetAndMagnitude.x,
		                             targetAndMagnitude.y,
		                             targetAndMagnitude.z);
		localGravity = (target - transform.position).normalized;
		localGravity *= targetAndMagnitude.w;
	}

	void CheckpointDataSave(){
		checkpointGravity = localGravity;
		checkpointPitch = pitch;
		checkpointYaw = yaw;
		checkpointPosition = transform.position;
		checkpointRotation = transform.rotation;
	}

	void CheckpointDataLoad(){
		localGravity = checkpointGravity;
		pitch = checkpointPitch;
		yaw = checkpointYaw;
		transform.position = checkpointPosition;
		transform.rotation = checkpointRotation;
	}

	void YouDied(){

	}

	void OnCollisionEnter(Collision col){
		//Needs to set isGrounded to true if the collision is at the player's feet
	}

	void OnCollisionStay(Collision col){
		//Constantly checks if there is a collision at the player's feet. If not, isGrounded = false.
	}

	void OnCollisionExit(Collision col){
		//Needs to set isGrounded to false if the collision is at the player's feet
	}
}
