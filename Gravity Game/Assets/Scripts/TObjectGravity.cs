﻿using UnityEngine;
using System.Collections;

public class TObjectGravity : MonoBehaviour {

	public Vector3 localGravity;
	private Color ObjectiveColorCode = Color.blue;
	//Checkpoint data
	private Vector3 checkpointGravity;
	private Vector3 checkpointPosition;
	private Quaternion checkpointRotation;

	// Use this for initialization
	void Start () {
		colorChildren (transform);
		/*
		try{
			gameObject.GetComponent<Renderer>().material.color = ObjectiveColorCode;
		} catch {

		}
		int count = 0;
		while (count < transform.childCount) {
			try{
				transform.GetChild(count).gameObject.GetComponent<Renderer>().material.color = ObjectiveColorCode;
			} catch {

			}
			count++;
		}
		*/
		if (localGravity == Vector3.zero) {
			localGravity = Vector3.down * 9.8f;
		}
		checkpointGravity = localGravity;
		checkpointPosition = transform.position;
		checkpointRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		GetComponent<Rigidbody>().velocity += localGravity * Time.fixedDeltaTime;
	}

	void Attract(Vector3 newGravity){
		localGravity = newGravity;
	}

	void colorChildren(Transform currentObject){
		try{
			currentObject.gameObject.GetComponent<Renderer>().material.color = ObjectiveColorCode;
		} catch {
			
		}
		int count = 0;
		while (count < transform.childCount) {
			try{
				colorChildren(currentObject.GetChild(count));
			} catch {

			}
			count++;
		}
	}

	void CheckpointDataSave(){
		checkpointGravity = localGravity;
		checkpointPosition = transform.position;
		checkpointRotation = transform.rotation;
	}

	void CheckpointDataLoad(){
		localGravity = checkpointGravity;
		transform.position = checkpointPosition;
		transform.rotation = checkpointRotation;
	}
}
